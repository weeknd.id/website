import {NextPageContext} from "next";
import SsoService from "services/sso";
import {setCookie} from "nookies";
import config from "config";
import constant from "common/constant";

export async function getServerSideProps(ctx: NextPageContext) {
  const {code}: any = ctx.query
  const ssoService: SsoService = new SsoService()

  const { error, data }: any = await ssoService.authenticate(code)
  if (error) {
    return {
      redirect: {
        destination: `/error?msg=${error}`,
        permanent: false
      }
    }
  }

  const opts: any = {
    domain: config.domain,
    maxAge: 86400,
    path: '/'
  }

  setCookie(ctx, constant.cookieTokenKey, data.accessToken, opts)

  return {
    redirect: {
      destination: `/platform`,
      permanent: false
    }
  }
}

export default function Oauth() {
  return (
    <></>
  )
}