import Head from "next/head";
import {NextPageContext} from "next";
import SsoService from "../services/sso";

export async function getServerSideProps(ctx: NextPageContext) {
  const ssoService: SsoService = new SsoService()

  return {
    props: {
      googleOauthUrl: ssoService.getGoogleLoginUrl()
    }
  }
}

export default function Home({ googleOauthUrl }: any) {
  const products: Array<any> = [
    {
      title: 'Marketify.',
      description: 'No more hiring developers to build your product landing page. Sign up, click-n-edit, then publish in less than 5 minutes.',
      partners: [
        { fullname: 'Ais', imageUrl: 'https://media-exp1.licdn.com/dms/image/C5103AQHTk0qh6nrt2Q/profile-displayphoto-shrink_200_200/0/1554980011583?e=1639612800&v=beta&t=-pHrjJqhGj6eOMNsdHMeq70c9XnlRDjzjYRaeNcxgTg' },
        { fullname: 'Putra Setia Utama', imageUrl: 'https://media-exp1.licdn.com/dms/image/C4D03AQH2vH90KMLIOw/profile-displayphoto-shrink_100_100/0/1517022238327?e=1639612800&v=beta&t=CPdVM88oJHUp3fluuHrwTJ72y_98xxG6GJjGH7Bd7dE' }
      ]
    },
    {
      title: 'Mediafy.',
      description: 'No more coding your upload API to Object Storage S3-Support. Sign up, configure base URL, access key, secret key, and you are good to go.',
      partners: [
        { fullname: 'Ais', imageUrl: 'https://media-exp1.licdn.com/dms/image/C5103AQHTk0qh6nrt2Q/profile-displayphoto-shrink_200_200/0/1554980011583?e=1639612800&v=beta&t=-pHrjJqhGj6eOMNsdHMeq70c9XnlRDjzjYRaeNcxgTg' }
      ]
    },
    {
      title: 'Payment.',
      description: 'No more account signup to Midtrans payment gateway for every project. One account for all project. Just hit our API.',
      partners: [
        { fullname: 'Ais', imageUrl: 'https://media-exp1.licdn.com/dms/image/C5103AQHTk0qh6nrt2Q/profile-displayphoto-shrink_200_200/0/1554980011583?e=1639612800&v=beta&t=-pHrjJqhGj6eOMNsdHMeq70c9XnlRDjzjYRaeNcxgTg' }
      ]
    },
    {
      title: 'Tickr.',
      description: 'No more hesitation in deciding when to entry or sell in Indonesia Stock Exchange (IDX/IHSG). Let our AI analyze the stock data for you.',
      partners: [
        { fullname: 'Ais', imageUrl: 'https://media-exp1.licdn.com/dms/image/C5103AQHTk0qh6nrt2Q/profile-displayphoto-shrink_200_200/0/1554980011583?e=1639612800&v=beta&t=-pHrjJqhGj6eOMNsdHMeq70c9XnlRDjzjYRaeNcxgTg' },
        { fullname: 'Asep Riyad', imageUrl: 'https://media-exp1.licdn.com/dms/image/C5603AQHzus7XR2beMg/profile-displayphoto-shrink_100_100/0/1627291714492?e=1639612800&v=beta&t=dPAVIgBO9KfRnaQl8cDopzQMbcBxr1ktFWpc0G9YZAc' }
      ]
    }
  ]

  return (
    <div>
      <Head>
        <title>Weeknd</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="description" content="Digital products that can help you make your work to be more productive with less effort." />

        <meta property="og:title" content="Weeknd" />
        <meta property="og:description" content="Digital products that can help you make your work to be more productive with less effort." />
        <meta property="og:url" content="https://weeknd.id" />
        <meta property="og:type" content="website" />
        <meta property="og:image" content="https://images.unsplash.com/photo-1498050108023-c5249f4df085?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTB8fHRlY2hub2xvZ3l8ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&q=60" />
        
        <meta name="twitter:card" content="product" />
        <meta name="twitter:title" content="Weeknd" />
        <meta name="twitter:description" content="Digital products that can help you make your work to be more productive with less effort." />
        <meta name="twitter:url" content="https://weeknd.id" />
        <meta name="twitter:image" content="https://images.unsplash.com/photo-1498050108023-c5249f4df085?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTB8fHRlY2hub2xvZ3l8ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&q=60" />
      </Head>
      <main className={`px-5 md:px-0`}>
        <div className={`grid grid-cols-1 md:grid-cols-2`}>
          <div className={`py-10 md:flex md:flex-col md:justify-center md:px-20 lg:px-32 text-center md:text-left`}>
            <div className={`font-bold font-damion text-6xl mb-5`}>Weeknd</div>
            <p>
              Welcome to the Weeknd project.
              It has been my passion to build digital products that can help you make your work to be more productive with less effort.
            </p>
            <p>
              With more than 10 years of experience in the industry, I partnered with awesome people that will help me build the right platform for you.
            </p>
            <p>
              And yes, there are new products in the development phase, so stay tuned!
            </p>
            <p className={`font-damion text-2xl`}>
              Ais
            </p>
            <div className={`mt-5`}>
              <a href={googleOauthUrl} className={`font-semibold bg-black hover:bg-indigo-600 text-white px-6 py-3 hover:text-white`}>Login/Sign Up using Google</a>
            </div>
          </div>
          <div
            className={`bg-center bg-cover`}
            style={{height: 400, backgroundImage: "url('https://images.unsplash.com/photo-1498050108023-c5249f4df085?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTB8fHRlY2hub2xvZ3l8ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&q=60')"}}>
          </div>
        </div>

        <div className={`container mx-auto py-20`}>
          <div className={`grid grid-cols-1 gap-5 md:grid-cols-4 md:gap-5`}>
            {products.map((product: any, i: number) => (
              <ProductContainer key={i} product={product} />
            ))}
          </div>
        </div>

        <div className={`container mx-auto pb-20 md:flex md:items-center`}>
          <div className={`font-bold text-2xl w-full text-center md:text-left md:w-5/6 mb-10`}>
            <span className={`md:block`}>We always love to hear anything from you. </span>
            If you have any questions or feedback, don&lsquo;t hesitate to drop us a message.
          </div>
          <div className={`w-full text-center md:text-left md:w-1/6`}>
            <a href={`mailto:hello.weeknd.id@gmail.com`} className={`bg-black text-white px-6 py-3 text-xl md:text-lg hover:bg-indigo-600 hover:text-white`}>Drop us a message</a>
          </div>
        </div>

        <footer className={`text-center text-xs py-4`}>
          &copy; 2021. Weeknd. All rights reserved.
        </footer>
      </main>
    </div>
  )
}

const ProductContainer = ({ product }: any) => {
  return (
    <a href={product.landingPageUrl ? product.landingPageUrl : '#'} className={`rounded p-5 group cursor-pointer block`}>
      <div className={`flex justify-center items-center mb-5 space-x-1`}>
        {product.partners.map((item: any, i: number) => (
          // eslint-disable-next-line @next/next/no-img-element
          <div key={i}><img src={item.imageUrl} alt={item.fullname} className={`w-10 h-10 rounded-full`} /></div>
        ))}
      </div>
      <div className={`text-center font-damion font-bold text-4xl group-hover:text-indigo-600`}>{product.title}</div>
      <div className={`mt-5 text-center text-black mb-5`}>{product.description}</div>

      {product.landingPageUrl ? (
        <div className={`flex justify-center`}>
          <div className={`rounded-full text-sm text-center text-indigo-600 group-hover:bg-indigo-600 group-hover:text-white flex justify-center items-center w-10 h-10`}>
            <i className={`fa fa-long-arrow-right`} />
          </div>
        </div>
      ):(
        <div className={`font-damion text-sm text-center mt-8`}>Coming Soon</div>
      )}
    </a>
  )
}