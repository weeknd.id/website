import {NextPageContext} from "next";
import {parseCookies} from "nookies";
import constant from "common/constant";
import jwt from "jsonwebtoken"
import config from "config"
import moment from "moment";
import ProfileRepository from "repositories/profile";

export async function getServerSideProps(ctx: NextPageContext) {
  const cookies: any = parseCookies(ctx)

  try {
    const accessToken: any = cookies[constant.cookieTokenKey]
    const account: any = jwt.verify(accessToken, config.jwt.secret, { algorithms: config.jwt.algorithm })
    if (account.exp < moment().unix()) {
      return {
        redirect: {
          destination: '/',
          permanent: false
        }
      }
    }

    const profileRepository: ProfileRepository = new ProfileRepository()
    const profile: any = await profileRepository.findByAccountId(account.id)
    const {firstName, lastName, phoneNo, metadata}: any = profile
    return {
      props: {
        firstName,
        lastName,
        phoneNo,
        picture: metadata.picture
      }
    }
  } catch (e) {
    return {
      redirect: {
        destination: '/',
        permanent: false
      }
    }
  }
}

export default function Platform() {
  return (
    <></>
  )
}