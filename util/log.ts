export default class LogUtil {
  private readonly className: string
  private method?: string
  private description?: string
  private errorMsg?: string
  private errorDetail: any

  constructor(className: string) {
    this.className = className
  }

  public setMethod(method: string) {
    this.method = method
    return this
  }

  public setDescription(description: string) {
    this.description = description
    return this
  }

  private constructMethod(): string {
    return `${this.className}.${this.method}`
  }

  public info(metadata: any) {
    console.dir({ type: 'info', method: this.constructMethod(), metadata: metadata ? metadata : {} })
  }

  public error(msg: string, e?: any) {
    this.errorMsg = msg.length ? msg : 'Something went wrong. Please try again later.'
    this.errorDetail = e

    let obj: any = {
      type: 'error',
      method: this.constructMethod(),
      description: this.description,
      error: e
    }

    console.dir(obj)
    return { error: this.errorMsg }
  }
}