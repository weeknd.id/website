const getValue: any = (variable: string, defaultValue: any) => process.env[variable] ? process.env[variable] : defaultValue
const getBooleanValue: any = (variable: string, defaultValue: boolean) => process.env[variable] ? JSON.parse(`${process.env[variable]}`) : defaultValue

const config = () => {
  return {
    isMaintenance: getBooleanValue('MAINTENANCE', false),
    domain: getValue('DOMAIN', 'localhost'),
    baseUrl: getValue('BASE_URL', 'http://localhost:5002'),

    jwt: {
      secret: getValue('JWT_SECRET', 'secret'),
      algorithm: getValue('JWT_ALGORITHM', 'HS256')
    },

    google: {
      clientId: getValue('GOOGLE_CLIENT_ID', ''),
      clientSecret: getValue('GOOGLE_CLIENT_SECRET', ''),
      redirectUri: getValue('GOOGLE_REDIRECT_URI', ''),
      oauthScope: getValue('GOOGLE_OAUTH_SCOPE', '')
    }
  }
}

export default config()