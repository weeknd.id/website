import db from "lib/database";
import moment from "moment";
import {v1 as uuidv1} from "uuid";

export default class ProfileRepository {
  constructor() { }

  public async findByAccountId(accountId: string) {
    return await db.profile.findFirst({ where: { accountId, deletedAt: null } })
  }

  public async insert(data: any) {
    data.id = uuidv1()
    data.createdAt = moment().format()
    return await db.profile.create({ data })
  }

  public async updateById(id: string, data: any) {
    data.updatedAt = moment().format()
    return await db.profile.update({ where: { id }, data })
  }

  public async deleteById(id: string) {
    return await db.profile.update({ where: { id }, data: { deletedAt: moment().format() }})
  }
}