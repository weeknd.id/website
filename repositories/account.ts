import db from "lib/database";
import moment from "moment";

export default class AccountRepository {
  constructor() { }

  public async findById(id: string) {
    return await db.account.findFirst({ where: { id, deletedAt: null } })
  }

  public async findByUsername(username: string) {
    return await db.account.findFirst({ where: { username, deletedAt: null } })
  }

  public async insert(data: any) {
    data.createdAt = moment().format()
    return await db.account.create({ data })
  }

  public async updateById(id: string, data: any) {
    data.updatedAt = moment().format()
    return await db.account.update({ where: { id }, data })
  }

  public async deleteById(id: string) {
    return await db.account.update({ where: { id }, data: { deletedAt: moment().format() }})
  }
}