export default {
  GoogleApiUrl: 'https://www.googleapis.com/oauth2',
  GoogleOAuthUrl: 'https://accounts.google.com/o/oauth2',
  GoogleTokenUrl: 'https://oauth2.googleapis.com/token',

  cookieTokenKey: 'weeknd_token',
}

