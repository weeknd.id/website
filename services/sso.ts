import config from "config";
import constant from "common/constant";
import Fetch from "util/fetch";
import AccountRepository from "repositories/account";
import jwt from "jsonwebtoken";
import ProfileRepository from "repositories/profile";
import moment from "moment";
import LogUtil from "util/log";

export default class SsoService {
  private log: LogUtil
  private accountRepository: AccountRepository
  private profileRepository: ProfileRepository

  constructor() {
    this.log = new LogUtil('SsoService')
    this.accountRepository = new AccountRepository()
    this.profileRepository = new ProfileRepository()
  }

  public getGoogleLoginUrl(): string {
    return `${constant.GoogleOAuthUrl}/auth?client_id=${config.google.clientId}&redirect_uri=${config.baseUrl}/oauth&scope=${config.google.oauthScope}&access_type=offline&response_type=code`
  }

  public async authenticate(code: string) {
    let log: LogUtil = this.log.setMethod('authenticate')

    try {
      const accessTokenUrl: string = `${constant.GoogleTokenUrl}?client_id=${config.google.clientId}&client_secret=${config.google.clientSecret}&redirect_uri=${config.google.redirectUri}&code=${code}&grant_type=authorization_code`
      const {access_token: accessToken}: any = await Fetch('POST', accessTokenUrl)
      if (!accessToken) {
        return log.setDescription('access token is undefined').error('Failed to authenticate. Please try again later.')
      }

      const profileUrl: string = `${constant.GoogleApiUrl}/v3/userinfo?alt=json&access_token=${accessToken}`
      const resultProfile: any = await Fetch('POST', profileUrl)
      if (!resultProfile) {
        return log.setDescription('unable to fetch profile from google api').error('Failed to authenticate. Please try again later.')
      }
      const {sub: id , name: fullName, given_name: firstName, family_name: lastName, picture, email, email_verified: emailVerified}: any = resultProfile
      const resultExistingAccount: any = await this.accountRepository.findByUsername(email)
      if (!resultExistingAccount) {
        await this.accountRepository.insert({ id, username: email })
        await this.profileRepository.insert({ account: { connect: { id } }, firstName, lastName, metadata: { picture, emailVerified }})
      }

      return {
        data: {
          accessToken: jwt.sign({ id, fullName, exp: moment().add(1, 'day').unix() }, config.jwt.secret)
        }
      }
    } catch (e) {
      return log.setDescription('throw error').error('Something went wrong. Please try again later.', e)
    }
  }
}